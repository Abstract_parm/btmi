class TelegramUser:
    tg_userid: int
    tg_username: str
    tg_fullname: str

    def __init__(self, id: int, username: str | None, fullname: str | None):
        self.tg_userid = id
        self.tg_username = username or ''
        self.tg_fullname = fullname or ''
