from os import environ

import httpx
from httpx import Response, codes, AsyncClient
from dotenv import load_dotenv

from src.controllers.redis_adapter import CacheService


class BinanceApiProvider:
    client: AsyncClient
    host: str
    bath_url: str
    token: str
    cacheService: CacheService

    def __init__(self):
        load_dotenv()
        self.client = AsyncClient()
        self.host = environ.get('APP_BINANCE_HOST') or ''
        self.bath_url = environ.get('APP_BINANCE_BATH_URL') or ''
        self.token = environ.get('APP_BINANCE_TOKEN')
        self.cacheService = CacheService()

    async def get_ticker_price(self, ticker: str) -> str:
        if not (price := self.cacheService.getPairPrice(ticker)):
            response = await self.__request_ticker_price(ticker)
            while response.status_code != codes.OK:
                response = await self.__request_ticker_price(ticker)
            price = response.json()['price']
            self.cacheService.savePairPrice(ticker, price)
        return price

    async def __request_ticker_price(self, ticker: str) -> Response:
        response = await self.client.get(
            f"{self.host}/{self.bath_url}/ticker/price",
            params={"symbol": ticker}
        )
        return response
