from aiogram import Router, F
from aiogram.filters import Command
from aiogram.types import Message, CallbackQuery

from keyboard import start_keyboard
from src.providers.binance_api_provider import BinanceApiProvider
from services.user_service import UserService
from dto.telegram_user import TelegramUser

router = Router()
binance = BinanceApiProvider()
userService = UserService()


@router.message(Command("start"))
async def start_handler(message: Message):
    await message.delete()
    await message.answer(
        text='Привет-привет',
        reply_markup=start_keyboard.getStartKeyboard()
    )
    user = TelegramUser(
        message.from_user.id,
        message.from_user.username,
        message.from_user.full_name
    )
    userService.saveUserByTelegramUser(user)


@router.message(F.text == 'Получить курс BTC к USD')
async def send_btc_price(message: Message):
    price = await binance.get_ticker_price('BTCTUSD')
    await message.answer(
        text=f"Текущий курс BTC к USD: <b>{price}</b>",
        reply_markup=start_keyboard.refreshPair()
    )


@router.callback_query(F.data == 'refresh_pair')
async def refresh_pair(callback: CallbackQuery):
    price = await binance.get_ticker_price('BTCTUSD')
    message = f"<b>Обновлено</b>:\nТекущий курс BTC к USD: <b>{price}</b>"
    if message != callback.message.html_text.strip():
        await callback.message.edit_text(
            message,
            reply_markup=start_keyboard.refreshPair()
        )
    else:
        await callback.message.delete()
        await callback.message.answer(
            message,
            reply_markup=start_keyboard.refreshPair()
        )
    await callback.answer()
