from aiogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup
from aiogram.utils.keyboard import ReplyKeyboardBuilder, InlineKeyboardBuilder


def getStartKeyboard() -> ReplyKeyboardMarkup:
    builder = ReplyKeyboardBuilder()
    builder.button(text='Получить курс BTC к USD')
    return builder.as_markup(resize_keyboard=True)


def refreshPair() -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    builder.button(text='Обновить', callback_data='refresh_pair')
    return builder.as_markup()
