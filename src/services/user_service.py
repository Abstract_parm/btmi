from sqlalchemy.orm import Session

from src.dto.telegram_user import TelegramUser
from src.entity.schema import User
from src.orm.sql import db


class UserService:
    session: Session

    def __init__(self):
        self.session = Session(bind=db)

    def saveUserByTelegramUser(self, tg_user: TelegramUser):
        if not self.getUserByTelegramUser(tg_user):
            user = User(
                tg_username=tg_user.tg_username,
                tg_fullname=tg_user.tg_fullname,
                tg_userid=tg_user.tg_userid
            )
            self.session.add(user)
            self.session.commit()
        else:
            self.session.query(User).filter_by(tg_userid=tg_user.tg_userid).\
                update({
                    User.tg_username: tg_user.tg_username,
                    User.tg_fullname: tg_user.tg_fullname
                })
            self.session.commit()

    def getUserByTelegramUser(self, user: TelegramUser) -> User|None:
        return ((self.session.query(User)
                .filter_by(tg_userid=user.tg_userid))
                .first())
