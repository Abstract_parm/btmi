import time
from os import environ

import redis, dotenv


class CacheService:
    cache_prefixes: dict = {
        "price": "PRICE_PAIR_PREFIX:"
    }
    redis: redis.StrictRedis

    def __init__(self):
        dotenv.load_dotenv()
        self.redis = redis.StrictRedis(
            environ.get('APP_REDIS_HOST'),
            int(environ.get('APP_REDIS_PORT')) or 6379,
            password=environ.get('APP_REDIS_PASSWORD') or '',
            decode_responses=True
        )

    def savePairPrice(self, pair: str, value: str):
        time.time()
        self.redis.set(
            f"{self.cache_prefixes['price']}{pair}",
            value,
            ex=environ.get('APP_REDIS_CACHE_TIME_SEC')
        )

    def getPairPrice(self, pair) -> str | None:
        return self.redis.get(f"{self.cache_prefixes['price']}{pair}")
