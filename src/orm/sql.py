from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from dotenv import load_dotenv
from os import environ
from src.entity.schema import Base


load_dotenv()
db = create_engine(f"sqlite:///{environ.get('APP_DB_PATH')}", echo=True)
Base.metadata.create_all(db)
session = Session(db, autoflush=True)

