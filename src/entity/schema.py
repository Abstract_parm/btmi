from sqlalchemy.orm import DeclarativeBase
from sqlalchemy import Column, Integer, String


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    tg_username = Column(String, default='')
    tg_fullname = Column(String, default='')
    tg_userid = Column(Integer, unique=True, nullable=False)
